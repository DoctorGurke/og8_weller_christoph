package main;

import java.util.Scanner;

public class BmiTest {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Bmi bmi = new Bmi();

		double weight = 0;
		double height = 0;
		String sex = "";
		double bmiValue = 0;

		// start der Abfrage
		System.out.println("Ihr Gewicht in kg:");
		weight = input.nextDouble();
		System.out.println("Ihre Körpergröße in cm:");
		height = input.nextDouble();
		System.out.println("Ihr Geschlecht (m/w):");
		switch (input.next()) {
		case "m":
			sex = "m";
			break;
		case "w":
			sex = "w";
			break;
		default:
			System.out.println("Ungültige Eingabe, Programm beendet.");
			break;
		}
		bmiValue = bmi.calcBmi(weight, height, sex);

		if (sex == "m" && bmiValue < 20 || sex == "w" && bmiValue < 19) {
			System.out.println("Sie haben einen BMI von " + bmiValue + ", womit Sie als untergewichtig gelten.");
		} else if (sex == "m" && bmiValue > 25|| sex == "w" && bmiValue > 24 ) {
			System.out.println("Sie haben einen BMI von " + bmiValue + ", womit Sie als übergewichtig gelten.");
		} else {
			System.out.println("Sie haben einen BMI von " + bmiValue + ", womit Sie als normalgewichtig gelten.");
		}

			System.exit(0);
		input.close();
	}

}
