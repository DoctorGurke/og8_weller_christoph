package main;

public class Bmi {
	public double calcBmi(double weight, double height, String sex) {
		height = height / 100;
		double bmi = weight / (height * height);
		return Math.round(bmi);
	}
}
