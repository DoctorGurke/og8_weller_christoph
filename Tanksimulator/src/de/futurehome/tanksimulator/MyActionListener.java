package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden) {
			System.exit(0);
		}

		double fuellstand = f.myTank.getFuellstand();
		int roundedFuellstand;
		int verbrauchAmount = f.verbrauchSlider.getValue();

		if (obj == f.btnEinfuellen) {
			if (f.myTank.getFuellstand() >= f.myTank.getMaxFuellstand() - 5) {
				fuellstand = f.myTank.getMaxFuellstand();
				f.lblFuellstand.setText(fuellstand + "l");
				f.lblFuellstandInfo.setText("Tank voll - 100%");
				System.out.println(LocalTime.now() + " | btnEinfuellen(5), Tankstand: 100l");
			} else {
				fuellstand = fuellstand + 5;
				f.lblFuellstand.setText(fuellstand + "l");
				f.lblFuellstandInfo.setText("Tank: " + fuellstand * (100 / f.myTank.getMaxFuellstand()) + "%");
				System.out.println(LocalTime.now() + " | btnEinfuellen(5), Tankstand: " + fuellstand + "l");
			}
			f.myTank.setFuellstand(fuellstand);
			roundedFuellstand = (int) Math.round(fuellstand * (100 / f.myTank.getMaxFuellstand()));
			f.balken.setValue(roundedFuellstand);

		}

		if (obj == f.btnVerbrauchen) {
			if (f.myTank.getFuellstand() < verbrauchAmount) {
				fuellstand = 0;
				f.lblFuellstand.setText(fuellstand + "l");
				f.lblFuellstandInfo.setText("Tank leer - 0%");
				System.out.println(LocalTime.now() + " | btnVerbrauchen(" + verbrauchAmount + "), Tankstand: 0l");
			} else {
				fuellstand = fuellstand - verbrauchAmount;
				f.lblFuellstand.setText(fuellstand + "l");
				f.lblFuellstandInfo.setText("Tank: " + fuellstand * (100 / f.myTank.getMaxFuellstand()) + "%");
				System.out.println(LocalTime.now() + " | btnVerbrauchen(" + verbrauchAmount + "), Tankstand: " + fuellstand + "l");
			}
			f.myTank.setFuellstand(fuellstand);
			roundedFuellstand = (int) Math.round(fuellstand);
			f.balken.setValue(roundedFuellstand);

		}

		if (obj == f.btnZuruecksetzen) {
			fuellstand = 0;
			f.myTank.setFuellstand(0);

			f.lblFuellstand.setText(fuellstand + "l");
			f.lblFuellstandInfo.setText("Tank leer - 0%");
			roundedFuellstand = (int) Math.round(fuellstand);
			f.balken.setValue(roundedFuellstand);
			System.out.println(LocalTime.now() + " | btnZuruecksetzen, Tankstand: 0l");
		}

	}
}