package de.futurehome.tanksimulator;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;

@SuppressWarnings("serial")
public class TankSimulator extends Frame {
	
	public Tank myTank;
	
	private Label lblUeberschrift = new Label("Tank-Simulator");
	public  Label lblFuellstand = new Label("0.0l  ");
	public Label lblFuellstandInfo = new Label("Tank Leer - 0%");
	
	public Button btnBeenden = new Button("Beenden");
	public Button btnEinfuellen = new Button("Einfüllen");
	public Button btnVerbrauchen = new Button("Verbrauchen");
	public Button btnZuruecksetzen = new Button("Zurücksetzen");
	
	private Panel pnlNorth = new Panel();
	private Panel pnlCenter = new Panel(new FlowLayout());
	private Panel pnlSouth = new Panel(new GridLayout(1, 0));

	private MyActionListener myActionListener = new MyActionListener(this);
	private final JPanel panel = new JPanel();
	private final JPanel panel_1 = new JPanel();
	private final JPanel panel_2 = new JPanel();
	private final JPanel panel_3 = new JPanel();
	private final JPanel panel_4 = new JPanel();
	
	JProgressBar balken = new JProgressBar(0);
	
	static final int VerbrauchMin = 1;
	static final int VerbrauchMax = 4;
	static final int VerbrauchInit = 2;    //initial frames per second
	JSlider verbrauchSlider = new JSlider(JSlider.HORIZONTAL,
	                                      VerbrauchMin, VerbrauchMax, VerbrauchInit);
	
	public TankSimulator() {
		super("Tank-Simulator");
		
		myTank = new Tank(0);
		
		this.lblUeberschrift.setFont(new Font("", Font.BOLD, 16));
		this.pnlNorth.add(this.lblUeberschrift);
		this.pnlSouth.add(this.btnEinfuellen);
		this.pnlSouth.add(this.btnVerbrauchen);
		this.pnlSouth.add(this.btnZuruecksetzen);
		this.pnlSouth.add(this.btnBeenden);
		this.add(this.pnlNorth, BorderLayout.NORTH);
		this.add(this.pnlCenter, BorderLayout.CENTER);
		
		verbrauchSlider.setMajorTickSpacing(1);
		verbrauchSlider.setPaintTicks(true);
		verbrauchSlider.setPaintLabels(true);
		
		balken.setValue(0);
		balken.setStringPainted(true);
		
		
		
		pnlCenter.add(panel);
		panel.setLayout(new GridLayout(4, 1, 0, 0));
		
		panel.add(panel_1);
		panel_1.add(lblFuellstand);
		
		panel.add(panel_2);
		panel_2.add(lblFuellstandInfo);
		
		panel.add(panel_3);
		panel_3.add(balken);
		
		panel.add(panel_4);
		panel_4.add(verbrauchSlider);
		
		
		this.add(this.pnlSouth, BorderLayout.SOUTH);
		this.pack();
		this.setVisible(true);
		
		// Ereignissteuerung
		this.btnEinfuellen.addActionListener(myActionListener);
		this.btnVerbrauchen.addActionListener(myActionListener);
		this.btnZuruecksetzen.addActionListener(myActionListener);
		this.btnBeenden.addActionListener(myActionListener);
	}

	public static void main(String argv[]) {
		TankSimulator f = new TankSimulator();
	}
}