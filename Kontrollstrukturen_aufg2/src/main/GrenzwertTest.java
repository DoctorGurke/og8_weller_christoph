package main;

import java.util.Scanner;

public class GrenzwertTest {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("n=");
		int n = input.nextInt();

		int sum = 0;
		String equation = "";

		// 2+4+6+...+2n
		// 1*2+2*2+3*2+4*2...+2n
		for (int i = 1; i <= n; i++) {
			sum = sum + (i * 2);

			// Gleichung mit letztem Rechenschritt ergänzen
			if (i == n) {
				equation = equation + Integer.toString(i * 2);
			} else {
				equation = equation + Integer.toString(i * 2) + "+";
			}

		}

		// Gleichung abschließen und ausgeben
		equation = equation + " = " + sum;
		System.out.println(equation);
		
		input.close();
		System.exit(0);

	}

}
