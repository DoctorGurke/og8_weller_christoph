/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor:
   Version: 1.0
   Datum:
*/
public class Fibonacci {
	// Konstruktor
	Fibonacci() {
	}

	/**
	 * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
	 * 
	 * @param n
	 * @return die n-te Fibonacci-Zahl
	 */
	long fiboRekursiv(int n) {
		if (n == 1 || n == 0)
			return 1;
		return fiboRekursiv(n - 1) + fiboRekursiv(n - 2);

		// vorl�ufig:
	}// fiboRekursiv

	/**
	 * Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
	 * 
	 * @param n
	 * @return die n-te Fibonacci-Zahl
	 */
	long fiboIterativ(int n) {
		// vorl�ufig:
		long temp1 = 0;
		long temp2 = 1;
		for(long i = 0; i < n; ++i) {
			long fib = temp1 + temp2;
			temp1 = temp2;
			temp2 = fib;
		}
		return temp2;
	}// fiboIterativ

}// Fibonnaci
