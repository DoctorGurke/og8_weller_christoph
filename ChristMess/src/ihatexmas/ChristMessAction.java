package ihatexmas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class ChristMessAction {
	
	public static void main(String[] args) {
		
		//Alle Kinder nach Bravheit auflisten
		//Kinder mit gleichem Bravheitsgrad nach Nachnamen auflisten
		
		//alle Kindobjekte mit positivem Bravheitsgrad ausgeben, alphabetisch nach Nachnamen sortiert
		System.out.println("Bei Macy's shoppen f�r:");	
		File file = new File("kinddaten.txt");
		KindGenerator kg = new KindGenerator();
		ArrayList<Kind> kinder = kg.instanciate(file);
		File fileKinder = new File("ooga.txt");
		ArrayList<Kind> kinderSorted = sortKinder(kinder, fileKinder);
		Collections.reverse(kinderSorted); //reserve um bravheit vom h�chsten zum niedrigsten aufzulisten
		for(Kind kind:kinderSorted) {
			if (kind.bravheit > 0) {
				System.out.println(kind.toString());
			}
			//System.out.println(kind.toString());
		}
	}
	
	public static ArrayList<Kind> sortKinder(ArrayList<Kind> kinder, File file) {
		for (int n = kinder.size(); n > 1; n--) {
			for (int i = 0; i < n-1; i++) {
				if ((kinder.get(i).compareTo(kinder.get(i+1)) > 0)) {
					Kind temp = kinder.get(i+1);
					kinder.set(i+1, kinder.get(i));
					kinder.set(i, temp);
				}
			}
		}
		write(kinder, file);
		return kinder;
	}
	
	

	public static void write(ArrayList<Kind> kinder, File file) {
		try {
			FileWriter fileWriter = new FileWriter(file, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			for (int i = 0; i < kinder.size(); i++) {
				bufferedWriter.write(kinder.get(i).toString());
				bufferedWriter.newLine();
				bufferedWriter.flush();
			}
			
			bufferedWriter.close();
		} catch (IOException e) {
			System.out.println("File does not exist");
		}
	}

}
