package ihatexmas;

import java.text.SimpleDateFormat;
import java.util.*;

public class Kind implements Comparable<Kind> {
	String vorname, nachname, ort;
	Date geburtstag;
	int bravheit;
	
	SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

	public Kind(String vorname, String nachname, Date datum, String bravheit, String ort) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtstag = datum;
		this.bravheit = Integer.parseInt(bravheit);
		this.ort = ort;
	}

	public String toString() {
		String date = dateFormatter.format(geburtstag);
		return "name: " + this.vorname + " " + this.nachname + "\t| datum: " + date + "\t| bravheit: " + Integer.toString(this.bravheit) + "\t| ort: " + this.ort;
	}
	
	@Override
	public int compareTo(Kind vergleich) {
		int intBravheit = Integer.compare(this.bravheit, vergleich.bravheit);
		int intOrt = vergleich.ort.compareTo(this.ort); //umgekehrt weil Strings anders sortiert werden
		
	    if(intBravheit != 0) {
	    	return intBravheit;
	    }
	    return intOrt;
	}

	
	
	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Date getGeburtstag() {
		return geburtstag;
	}

	public void setGeburtstag(Date geburtstag) {
		this.geburtstag = geburtstag;
	}

	public int getBravheit() {
		return bravheit;
	}

	public void setBravheit(int bravheit) {
		this.bravheit = bravheit;
	}	
	
}
