package ihatexmas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class KindGenerator {

	public ArrayList<Kind> instanciate(File file) {
		String[] kids = null;
		String[] attributes = null;
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String string;
			SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
			ArrayList<Kind> kinder = new ArrayList<Kind>();
			while ((string = bufferedReader.readLine()) != null) {
				kids = string.split("[A-Z]\\) ");
				String nachname = "";
				String vorname = "";
				String geburtstag = "";
				String bravheit = "";
				String ort = "";
				String temp = "";
				Date date = null;
				for (int i = 0; i < kids.length; i++) {
					attributes = kids[i].split(" ");
					vorname = attributes[0];
					nachname = attributes[1];
					geburtstag = attributes[2];
					bravheit = attributes[3];
					for (int j = 0; j < attributes.length - 4; j++) {
						temp = temp + attributes[j + 4] + " ";
					}
					ort = temp;

					if (vorname.endsWith(","))
						vorname = vorname.substring(0, vorname.length() - 1);
					if (nachname.endsWith(","))
						nachname = nachname.substring(0, nachname.length() - 1);
					if (geburtstag.endsWith(","))
						geburtstag = geburtstag.substring(0, geburtstag.length() - 1);
					if (bravheit.endsWith(","))
						bravheit = bravheit.substring(0, bravheit.length() - 1);
					if (ort.endsWith(","))
						ort = ort.substring(0, ort.length() - 1);
					
					date = dateFormatter.parse(geburtstag);

				}
				Kind kind = new Kind(vorname, nachname, date, bravheit, ort);
				kinder.add(kind);
				//System.out.println(kind.toString());
			}
			bufferedReader.close();
			return kinder;

		} catch (Exception e) {
			System.out.println("error: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
}