package main;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		Taschenrechner calc = new Taschenrechner();

		int swValue;

		// Display menu graphics
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Multiplizieren |");
		System.out.println("|        4. Dividieren     |");
		System.out.println("|        5. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = input.next().charAt(0);
		double temp1 = 0;
		double temp2 = 0;
		// Switch construct
		switch (swValue) {
		case '1':
			System.out.println("Erster Summand:");
			temp1 = input.nextDouble();
			System.out.println("Zweiter Summand:");
			temp2 = input.nextDouble();
			System.out.println(Math.round(temp1) + " + " + Math.round(temp2) + " = " + calc.add(temp1, temp2));
			break;

		case '2':
			System.out.println("Minuend:");
			temp1 = input.nextDouble();
			System.out.println("Subtrahend:");
			temp2 = input.nextDouble();
			System.out.println(Math.round(temp1) + " - " + Math.round(temp2) + " = " + calc.sub(temp1, temp2));
			break;

		case '3':
			System.out.println("Multiplikant:");
			temp1 = input.nextDouble();
			System.out.println("Multiplikator:");
			temp2 = input.nextDouble();
			System.out.println(Math.round(temp1) + " * " + Math.round(temp2) + " = " + calc.mul(temp1, temp2));
			break;

		case '4':
			System.out.println("Dividend:");
			temp1 = input.nextDouble();
			System.out.println("Divisor:");
			temp2 = input.nextDouble();
			System.out.println(Math.round(temp1) + " / " + Math.round(temp2) + " = " + calc.div(temp1, temp2));
			break;

		case '5':
			System.exit(0);
			break;

		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
		}

		input.close();

	}

}