package main;

public class Addon {

	private int id;
	private String bezeichnung;
	private double preis;
	private int bestand;
	private int maxBestand;

	public Addon() {

	}

	public void setID(int id) {
		this.id = id;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public void setBestand(int bestand) {
		if (bestand > this.maxBestand) {} else {
			this.bestand = bestand;
		}
	}

	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}

	public int getID() {
		return this.id;
	}

	public String getBezeichnung() {
		return this.bezeichnung;
	}

	public double getPreis() {
		return this.preis;
	}

	public int getBestand() {
		return this.bestand;
	}

	public int getMaxBestand() {
		return this.maxBestand;
	}

	public double berechneGesamtwert() {
		return this.bestand * this.preis;
	}

}
