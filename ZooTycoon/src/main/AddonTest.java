package main;

public class AddonTest {

	public static void main(String[] args) {
		
		Addon addon = new Addon();
		
		addon.setID(1);
		addon.setBezeichnung("Spezialfutter");
		addon.setPreis(0.29);
		addon.setBestand(1);
		addon.setMaxBestand(5);
		
		System.out.println("Id: "+addon.getID());
		System.out.println("Bezeichnung: "+addon.getBezeichnung());
		System.out.println("Preis: "+addon.getPreis());
		System.out.println("Bestand: "+addon.getBestand());
		System.out.println("Max Bestand: "+addon.getMaxBestand());
		
		addon.setBestand(3);
		System.out.println("Bestand: " + addon.getBestand());
		System.out.println("Gesamtwert: " + addon.berechneGesamtwert());

	}

}
