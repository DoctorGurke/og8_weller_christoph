package main;

import java.util.Scanner;

public class QuotSub {

	public static void main(String[] args) {

		// Dividend : Divisor = Quotient

		Scanner input = new Scanner(System.in);
		int dividend = 0;
		int divisor = 0;

		// Wertangabe
		System.out.println("Berechnung des Quotienten und Rest �ber Subtraktion");
		System.out.println("Dividend:");
		dividend = input.nextInt();
		System.out.println("Divisor:");
		divisor = input.nextInt();

		input.close();

		// Magische Box, die viel Googlen beansprucht hat
		int quotient = 0;
		while (dividend >= divisor) {
			dividend = dividend - divisor;
			quotient++;
		}
		System.out.println("Ganzzahliger Quotient: " + quotient);
		System.out.println("Ganzzahliger Rest: " + dividend);
		
		System.exit(0);

	}

}
