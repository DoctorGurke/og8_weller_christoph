package main;

import java.util.Scanner;

public class RechnungenUtilTest {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		RechnungenUtil util = new RechnungenUtil();
		
		//Abfrage der Menge
		System.out.println("Anzahl der M�use:");
		int amount = input.nextInt();
		
		double price = 0;
		
		//Iterate durch die Menge an M�usen, um ihren Einzelpreis abzufragen.
		for (int i = 1; i <= amount; i++) {
			System.out.println("Einzelpreis der " + i + ". Maus:");
			price = price + input.nextInt();
		}
		
		//Abfrage der mwst in %
		System.out.println("MwSt. %:");
		double mwst = input.nextDouble();
		
		//Lieferaufschlag wird berechnet
		if (amount < 10) {
			price = price + 10;
			System.out.println("Lieferpauschale von 10� erhoben.");
		}
		
		//Endergebnis wird ausgegeben und resourcen werden beendet.
		System.out.println("Endbetrag, inkl. MwSt.: " + util.mwst(price, mwst) + "�");	
		input.close();
		System.exit(0);
	}
}
