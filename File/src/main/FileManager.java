package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileReader;

public class FileManager {
	private File file;
	
	public FileManager(File file) {
		this.file = file;
		
	}
	
	public void write(String string) {
		try {
			FileWriter fileWriter = new FileWriter(this.file, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(string);
			bufferedWriter.newLine();
			bufferedWriter.flush();
			bufferedWriter.close();
		} catch (IOException e) {
			System.out.println("File does not exist");
		}
	}
	
	public void read() {
		try {
			FileReader fileReader = new FileReader(this.file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String string;
			while ((string = bufferedReader.readLine()) != null) {
				System.out.println(string);
			}
		} catch (Exception e) {
			System.out.println("File does not exist");
		}
	}

	public static void main(String[] args) {
		File file = new File("OG8.txt");
		FileManager fileManager = new FileManager(file);
		//String string = "";
		int count = 0;
		for (int j = 2; count <= 1000000; ++j) {
			if(istPrim(j)) {
				//string = string + j + "\n";
				fileManager.write(Integer.toString(j));
				++count;
			}
		}
		
	}
	
	public static boolean istPrim(int n) {
		for (int k = 2; k <= Math.sqrt(n); ++k) {
			if(n%k == 0) 
				return false;
		}
		return true;
	}

}
