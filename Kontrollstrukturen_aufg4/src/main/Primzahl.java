package main;

import java.util.Scanner;

public class Primzahl {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		long num = 0;

		System.out.println("Geben Sie eine Zahl an:");
		num = input.nextLong();
		input.close();

		long temp = 0;
		boolean prime = true;

		for (int i = 2; i <= num / 2; i++) {
			temp = num % i;
			if (temp==0) {
				prime = false;
				break;
			}
		}
		if (prime) {
			System.out.println(num + " ist eine Primzahl.");
		} else {
			System.out.println(num + " ist keine Primzahl.");
		}

	}

}
