package main;

public class Sort {
	public int[] quickSort(int array[], int start, int end) {
	    if (start < end) {
	        int partitionIndex = partition(array, start, end);

	        quickSort(array, start, partitionIndex-1);
	        quickSort(array, partitionIndex+1, end);
	    }
	    return array;
	}
	
	private int partition(int[] array, int start, int end) {
	    int pivot = array[end];
	    int i = (start-1);

	    for (int j = start; j < end; j++) {
	        if (array[j] <= pivot) {
	            i++;

	            int swapTemp = array[i];
	            array[i] = array[j];
	            array[j] = swapTemp;
	        }
	    }

	    int swapTemp = array[i+1];
	    array[i+1] = array[end];
	    array[end] = swapTemp;
	    System.out.println("swap " + array[i+1] + " mit " + array[end]);
	    return i+1;
	}

}
