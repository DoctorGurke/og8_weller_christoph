package main;

public class Fahrer {
	private String name;
	private Auto auto;
	
	Fahrer(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Auto getAuto() {
		return auto;
	}

	public void setAuto(Auto auto) {
		this.auto = auto;
		auto.setFahrer(this);
	}
	
	public void unlinkAuto() {
		this.auto = null;
	}
	
	public void removeAuto() {
		this.auto.setFahrer(null);
		setAuto(null);
	}
	
}
