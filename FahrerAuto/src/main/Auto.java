package main;

public class Auto {
	private String kennzeichen;
	private Fahrer fahrer;
	
	public Auto(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}
	
	public String getKennzeichen() {
		return kennzeichen;
	}
	
	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}
	
	public Fahrer getFahrer() {
		return fahrer;
	}
	
	public void setFahrer(Fahrer fahrer) {
		this.fahrer = fahrer;
	}
	
	public void unlinkFahrer() {
		this.fahrer = null;
	}
	
	public void removeFahrer() {
		this.fahrer.setAuto(null);
		setFahrer(null);
	}
	
}
