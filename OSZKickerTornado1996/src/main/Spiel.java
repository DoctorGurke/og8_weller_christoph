package main;

public class Spiel {
	private Mannschaft gastMannschaft;
	private Mannschaft heimMannschaft;
	private int gastTore;
	private int heimTore;
	private Spieler[] gelbeKarten;
	private Spieler[] roteKarten;
	private String stattgefunden;
	
	public Spiel(Mannschaft gastMannschaft, Mannschaft heimMannschaft, int gastTore, int heimTore, Spieler[] gelbeKarten, Spieler[] roteKarten, String stattgefunden) {
		this.gastMannschaft = gastMannschaft;
		this.heimMannschaft = heimMannschaft;
		this.gastTore = gastTore;
		this.heimTore = heimTore;
		this.gelbeKarten = gelbeKarten;
		this.roteKarten = roteKarten;
		this.stattgefunden = stattgefunden;
	}

	public Mannschaft getGastMannschaft() {
		return gastMannschaft;
	}
	
	public void setGastMannschaft(Mannschaft gastMannschaft) {
		this.gastMannschaft = gastMannschaft;
	}
	
	public Mannschaft getHeimMannschaft() {
		return heimMannschaft;
	}
	
	public void setHeimMannschaft(Mannschaft heimMannschaft) {
		this.heimMannschaft = heimMannschaft;
	}
	
	public int getGastTore() {
		return gastTore;
	}
	
	public void setGastTore(int gastTore) {
		this.gastTore = gastTore;
	}
	
	public int getHeimTore() {
		return heimTore;
	}
	
	public void setHeimTore(int heimTore) {
		this.heimTore = heimTore;
	}
	
	public Spieler[] getGelbeKarten() {
		return gelbeKarten;
	}
	
	public void setGelbeKarten(Spieler[] gelbeKarten) {
		this.gelbeKarten = gelbeKarten;
	}
	
	public Spieler[] getRoteKarten() {
		return roteKarten;
	}
	
	public void setRoteKarten(Spieler[] roteKarten) {
		this.roteKarten = roteKarten;
	}
	
	public String getStattgefunden() {
		return stattgefunden;
	}
	
	public void setStattgefunden(String stattgefunden) {
		this.stattgefunden = stattgefunden;
	}
	
	public void addGelbeKarte(Spieler spieler) {
		for(int i = 0; i < this.gelbeKarten.length; i++) {
			if (this.gelbeKarten[i] == null) {
				this.gelbeKarten[i] = spieler;
				break;
			}
		}
	}
	
	public void removeGelbeKarte(Spieler spieler) {
		for(int i = 0; i < this.gelbeKarten.length; i++) {
			if (this.gelbeKarten[i] == spieler) {
				this.gelbeKarten[i] = null;
				break;
			}
		}
	}
	
	public void addRoteKarte(Spieler spieler) {
		for(int i = 0; i < this.roteKarten.length; i++) {
			if (this.roteKarten[i] == null) {
				this.roteKarten[i] = spieler;
				break;
			}
		}
	}
	
	public void removeRoteKarte(Spieler spieler) {
		for(int i = 0; i < this.roteKarten.length; i++) {
			if (this.roteKarten[i] == spieler) {
				this.roteKarten[i] = null;
				break;
			}
		}
	}
	
}
