package main;

public class Schiedsrichter extends Person {

	private int gepfiffeneSpiele;

	public Schiedsrichter(String name, String telefon, boolean jahresbeitragBezahlt, int gepfiffeneSpiele) {
		super(name, telefon, jahresbeitragBezahlt);
		this.setGepfiffeneSpiele(gepfiffeneSpiele);
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

}
