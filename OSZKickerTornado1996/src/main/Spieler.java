package main;

public class Spieler extends Person {

	private int trikotNummer;
	private String spielPosition;
	private Mannschaft mannschaft;

	public Spieler(String name, String telefon, boolean jahresbeitragBezahlt, int trikotNummer, String spielPosition) {
		super(name, telefon, jahresbeitragBezahlt);
		this.trikotNummer = trikotNummer;
		this.spielPosition = spielPosition;
	}

	public int getTrikotNummer() {
		return trikotNummer;
	}

	public void setTrikotNummer(int trikotNummer) {
		this.trikotNummer = trikotNummer;
	}

	public String getSpielPosition() {
		return spielPosition;
	}

	public void setSpielPosition(String spielPosition) {
		this.spielPosition = spielPosition;
	}

	public Mannschaft getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft mannschaft) {
		if(this.mannschaft == null) {
			this.mannschaft = mannschaft;
		}
	}

	public void removeMannschaft() {
		this.mannschaft = null;
	}
	
}
