package main;

public class Person {
	private String name;
	private String telefon;
	private boolean jahresbeitragBezahlt;

	public Person(String name, String telefon, boolean jahresbeitragBezahlt) {
		super();
		this.name = name;
		this.telefon = telefon;
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public boolean isJahresbeitragBezahlt() {
		return jahresbeitragBezahlt;
	}

	public void setJahresbeitragBezahlt(boolean jahresbeitragBezahlt) {
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}

}
