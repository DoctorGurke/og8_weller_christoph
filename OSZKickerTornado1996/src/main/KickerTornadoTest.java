package main;

public class KickerTornadoTest {

	public static void main(String[] args) {
		Spieler spieler1 = new Spieler("Jens Müller", "01764111383", true, 11, "AUßenverteidiger"); //String name, String telefon, boolean jahresbeitragBezahlt, int trikotNummer, String spielPosition
		Spieler spieler2 = new Spieler("Hans Dieter", "01764453563", true, 6, "Torwart");
		Spieler spieler3 = new Spieler("Karl Herbert", "01764127333", false, 7, "Innenverteidiger");
		Spieler spieler4 = new Spieler("Tobias Heinrich", "01764368789", true, 3, "Torwart");
		
		Trainer trainer1 = new Trainer("Chris Klopp", "01752339864", true,'b' , 150);//String name, String telefon, boolean jahresbeitragBezahlt, char lizenzKlasse, int aufwandsEntschaedigung
		Trainer trainer2 = new Trainer("Fabian Heike", "01742339476", true,'c' , 130);
		
		Mannschaft mannschaft1 = new Mannschaft("GermanStarz", "A"); //String name, String spielklasse
		Mannschaft mannschaft2 = new Mannschaft("KickerGoalies", "B");
		
		mannschaft1.setTrainer(trainer1);
		mannschaft1.addSpieler(spieler1);
		mannschaft1.addSpieler(spieler2);
		
		mannschaft2.setTrainer(trainer2);
		mannschaft2.addSpieler(spieler3);
		mannschaft2.addSpieler(spieler4);
		
		System.out.println(mannschaft1.toString());
		System.out.println(mannschaft2.toString());
		System.out.println("");
		
		mannschaft1.removeSpieler(spieler1);
		mannschaft2.removeSpieler(spieler3);
		
		mannschaft1.addSpieler(spieler3);
		mannschaft2.addSpieler(spieler1);
		
		System.out.println(mannschaft1.toString());
		System.out.println(mannschaft2.toString());
		System.out.println("");
		
		mannschaft1.addSpieler(spieler1);
		
		System.out.println(mannschaft1.toString());
	}

}
