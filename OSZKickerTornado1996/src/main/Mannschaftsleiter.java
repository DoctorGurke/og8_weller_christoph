package main;

public class Mannschaftsleiter extends Spieler {

	private String mannschaftsName;
	private int rabatt;
	private Mannschaft mannschaft;

	public Mannschaftsleiter(String name, String telefon, boolean jahresbeitragBezahlt, int trikotNummer,
			String spielPosition, String mannschaftsName, int rabatt, Mannschaft mannschaft) {
		super(name, telefon, jahresbeitragBezahlt, trikotNummer, spielPosition);
		this.mannschaftsName = mannschaftsName;
		this.rabatt = rabatt;
		this.mannschaft = mannschaft;
	}

	public String getMannschaftsName() {
		return mannschaftsName;
	}

	public void setMannschaftsName(String mannschaftsName) {
		this.mannschaftsName = mannschaftsName;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
	public Mannschaft getMannschaft() {
		return mannschaft;
	}

	public void setMannschaft(Mannschaft mannschaft) {
		this.mannschaft = mannschaft;
	}

}
