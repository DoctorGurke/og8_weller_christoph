package main;

public class Mannschaft {
	private String name;
	private String spielklasse;
	private Spieler[] spieler = new Spieler[22];
	private Trainer trainer;
	
	Mannschaft(String name, String spielklasse){
		this.name = name;
		this.spielklasse = spielklasse;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpielklasse() {
		return spielklasse;
	}

	public void setSpielklasse(String spielklasse) {
		this.spielklasse = spielklasse;
	}

	public Spieler[] getSpieler() {
		return spieler;
	}

	public void setSpieler(Spieler[] spieler) {
		this.spieler = spieler;
	}
	
	public void addSpieler(Spieler spieler) {
		for(int i = 0; i < this.spieler.length; i++) {
			if (this.spieler[i] == null) {
				if(spieler.getMannschaft() == null) {
					this.spieler[i] = spieler;
					spieler.setMannschaft(this);
				} else {	
					System.out.println("Spieler ist bereits in einem Team!");
				}
				break;
			}
		}
	}
	
	public void removeSpieler(Spieler spieler) {
		for(int i = 0; i < this.spieler.length; i++) {
			if (this.spieler[i] == spieler) {
				this.spieler[i].removeMannschaft();
				this.spieler[i] = null;
				break;
			}
		}
	}
	
	public String getSpielerString() {
		String temp = "";
		for(int i = 0; i < this.spieler.length; i++) {
			if(this.spieler[i] != null) {
				temp = temp + this.spieler[i].getName() + ", ";
			}
		}
		return temp;
	}

	@Override
	public String toString() {
		return "Mannschaft [name=" + name + ", spielklasse=" + spielklasse + ", spieler=" + getSpielerString() + ", trainer=" + trainer.getName() + "]";
	}
	
	
	
}
