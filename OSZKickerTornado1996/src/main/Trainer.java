package main;

public class Trainer extends Person {

	private char lizenzKlasse;
	private int aufwandsEntschaedigung;

	public Trainer(String name, String telefon, boolean jahresbeitragBezahlt, char lizenzKlasse, int aufwandsEntschaedigung) {
		super(name, telefon, jahresbeitragBezahlt);
		this.lizenzKlasse = lizenzKlasse;
		this.aufwandsEntschaedigung = aufwandsEntschaedigung;
	}

	public char getLizenzKlasse() {
		return lizenzKlasse;
	}

	public void setLizenzKlasse(char lizenzKlasse) {
		this.lizenzKlasse = lizenzKlasse;
	}

	public int getAufwandsEntschaedigung() {
		return aufwandsEntschaedigung;
	}

	public void setAufwandsEntschaedigung(int aufwandsEntschaedigung) {
		this.aufwandsEntschaedigung = aufwandsEntschaedigung;
	}

}
