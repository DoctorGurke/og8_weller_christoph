package main;

public class MainTest {
	public static void main(String[] args) {
		Sort sort = new Sort();
		RandListGenerator randList = new RandListGenerator();
		int[] list = randList.generate(20);
		System.out.println("SelectionSort: ");
		System.out.println("unsortiert " + java.util.Arrays.toString(list));
		System.out.println("sortiert " + java.util.Arrays.toString(sort.selectionSort(list)));
		
	}
}
