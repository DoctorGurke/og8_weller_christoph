package main;

public class MainTest {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		final int[] LAENGEN = { 15000000, 16000000, 17000000, 18000000, 19000000, 20000000 };

		ListGenerator listgen = new ListGenerator();
		Search search = new Search();
		
		for (int i = 0; i < LAENGEN.length; i++) {
			System.out.println("#####" + LAENGEN[i] + "#####");
			long[] a = listgen.getSortedList(LAENGEN[i]);

			final int WORSTCASELINEAR = LAENGEN[i] - 1;
			final int AVGCASELINEAR = LAENGEN[i] / 2 - 1;
			final int BESTCASELINEAR = 0;
			final int NOCASELINEAR = -1;
			System.out.println("WORST CASE");
			for (int j = 0; j < 5; j++) {
				long gesuchteZahl = a[(int) WORSTCASELINEAR];
				search.SearchElementLinear(a, gesuchteZahl);
			}
			System.out.println();
			System.out.println("AVG CASE");
			for (int j = 0; j < 5; j++) {
				long gesuchteZahl = a[(int) AVGCASELINEAR];
				search.SearchElementLinear(a, gesuchteZahl);
			}
			System.out.println();
			System.out.println("BEST CASE");
			for (int j = 0; j < 5; j++) {
				long gesuchteZahl = a[(int) BESTCASELINEAR];
				search.SearchElementLinear(a, gesuchteZahl);
			}
			System.out.println();
			System.out.println("NO CASE");
			for (int j = 0; j < 5; j++) {
				long gesuchteZahl = NOCASELINEAR;
				search.SearchElementLinear(a, gesuchteZahl);
			}
			System.out.println();
		}
		
		for (int i = 0; i < LAENGEN.length; i++) {
			System.out.println("#####" + LAENGEN[i] + "#####");
			long[] a = listgen.getSortedList(LAENGEN[i]);

			final int WORSTCASEBINARY = (LAENGEN[i] / 4) * 3 + 1;
			final int AVGCASEBINARY = LAENGEN[i] / 3 + 1;
			final int BESTCASEBINARY = LAENGEN[i] / 2 + 1;
			final int NOCASEBINARY = -1;
			System.out.println("WORST CASE");
			for (int j = 0; j < 5; j++) {
				long gesuchteZahl = a[(int) WORSTCASEBINARY];
				search.test(a, 0, LAENGEN[i], gesuchteZahl);
			}
			System.out.println();
			System.out.println("AVG CASE");
			for (int j = 0; j < 5; j++) {
				long gesuchteZahl = a[(int) AVGCASEBINARY];
				search.test(a, 0, LAENGEN[i], gesuchteZahl);
			}
			System.out.println();
			System.out.println("BEST CASE");
			for (int j = 0; j < 5; j++) {
				long gesuchteZahl = a[(int) BESTCASEBINARY];
				search.test(a, 0, LAENGEN[i], gesuchteZahl);
			}
			System.out.println();
			System.out.println("NO CASE");
			for (int j = 0; j < 5; j++) {
				long gesuchteZahl = NOCASEBINARY;
				search.test(a, 0, LAENGEN[i], gesuchteZahl);
			}
			System.out.println();
		}

	}

}
