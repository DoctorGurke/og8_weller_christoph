package main;

public class Search {

	Stoppuhr time = new Stoppuhr();

	public void SearchElementLinear(long[] list, long gesuchteZahl) {
		time.start();
		for (int i = 0; i < list.length; i++) {
			if (list[i] == gesuchteZahl) {
				break;
			}
		}
		time.stopp();
		System.out.println(time.getDauerMs() + "ms");
	}

	public void SearchElementBinary(long[] list, int start, int ende, long zahl) {
		int grenze = start + ((ende - start) / 2);

		if (grenze >= list.length) {
			System.out.println(zahl + " nicht im Array enthalten.");
			return;
		}

		if (zahl > list[grenze]) {
			SearchElementBinary(list, grenze + 1, ende, zahl);
		} else if (zahl < list[grenze] && start != grenze) {
			SearchElementBinary(list, start, grenze - 1, zahl);
		} else if (zahl == list[grenze]) {
			time.stopp();
			System.out.println(zahl + " an Position " + grenze);

		} else {
			time.stopp();
			System.out.println(zahl + " nicht im Array enthalten.");
		}
	}
	
	public void test(long[] list, int start, int ende, long zahl) {
		time.start();
		SearchElementBinary(list, start, ende, zahl);
		time.stopp();
		System.out.println(time.getDauerMs() + "ms");
	}

}
