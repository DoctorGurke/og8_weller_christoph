package main;
public class Sort {
	Stoppuhr time = new Stoppuhr();
	public int[] bubleSort(int[] array) {
		time.start();
		System.out.println(java.util.Arrays.toString(array));
		int vergleiche = 0;
		for (int n = array.length; n > 1; n--) {
			for (int i = 0; i < n-1; i++) {
				vergleiche++;
				if (array[i] > array[i+1]) {
					int temp = array[i+1];
					array[i+1] = array[i];
					array[i] = temp;
				}
			}
		}
		time.end();
		System.out.println(vergleiche);
		System.out.println(time.timeMillis() + "ms");
		System.out.println(java.util.Arrays.toString(array));
		return array;
	}
}
