package main;

import java.util.Scanner;

public class PrimZahlen {

	static Stoppuhr time = new Stoppuhr();
	static long tempTime;
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		long[] testZahlen = { 99999999999999L };
		int testLauefe = 3;

		long tempTimeAvg = 0;
		for (int i = 0; i < testZahlen.length; i++) {
			for (int j = 0; j < testLauefe; j++) {
				System.out.println(istPrimZahl(testZahlen[i]));
				tempTimeAvg = tempTimeAvg + tempTime;
			}
			System.out.println("Avg time: " + (tempTimeAvg / testLauefe) + "ms");
			tempTimeAvg = 0;
		}
		System.out.println("Test beendet");

	}

	public static boolean istPrimZahl(long n) {
		time.start();
		boolean returnBool = true;
		if (n < 2)
			return false;
		for (long i = 2; i < Math.sqrt(n); i++) {
			if (n % i == 0) {
				returnBool = false;
				break;
			}
		}
		time.stopp();
		System.out.println(n + " in " + time.getDauerMs() + "ms gepr�ft");
		tempTime = time.getDauerMs();
		return returnBool;
	}
}
