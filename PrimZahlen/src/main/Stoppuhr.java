package main;

public class Stoppuhr {
	long startzeit;
	long endzeit;
	
	public void start() {
		this.startzeit = System.currentTimeMillis();
	}
	
	public void stopp() {
		this.endzeit = System.currentTimeMillis();
	}
	
	public long getDauerMs() {
		return (this.endzeit - this.startzeit);
	}
	
}
