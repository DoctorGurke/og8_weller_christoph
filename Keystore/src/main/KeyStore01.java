package main;

public class KeyStore01 {

	private String[] array;

	public KeyStore01() {
		array = new String[100];
	}

	public KeyStore01(int length) {
		array = new String[length];
	}

	public boolean add(String e) {
		for (int i = 0; i < array.length; ++i) {
			if (array[i] == null) {
				array[i] = e;
				return true;
			}
			if (i == array.length && array[i] != null) {
				break;
			}
		}
		return false;
	}

	public void clear() {
		for (int i = 0; i < array.length; ++i) {
			array[i] = null;
		}
	}

	public String get(int i) {
		return array[i];
	}

	public int indexOf(String search) {
		for (int i = 0; i < array.length; ++i) {
			if (array[i] != null && array[i].toUpperCase().equals(search.toUpperCase())) {
				return i;
			}
		}
		return -1;
	}

	public boolean remove(int i) {
		if (array[i] != null) {
			array[i] = null;
			return true;
		}
		return false;
	}

	public boolean remove(String search) {
		for (int i = 0; i < array.length; ++i) {
			if (array[i] != null && array[i].toUpperCase().equals(search.toUpperCase())) {
				array[i] = null;
				return true;
			}
		}
		return false;
	}

	public int size() {
		int count = 0;
		for (int i = 0; i < array.length; ++i) {
			if (array[i] != null) {
				++count;
			}
		}
		if (count < 0 || count > Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		}
		return count;
	}

	public String toString() {
		String arrayString = "[ ";
		if (this.size() > 0) {
			for (int i = 0; i < array.length; i++) {
				if (array[i] != null) {
					if (arrayString.contentEquals("[ ")) {
						arrayString = arrayString + array[i] + " ";
					} else {
						arrayString = arrayString + "," + array[i] + " ";
					}

				}
			}
		} else {
			return "Array empty";
		}
		arrayString = arrayString + " ]";
		return arrayString;

	}
}
