package main;

import java.util.ArrayList;

public class KeyStore02 {

	//private String[] array;
	private ArrayList<String> array = new ArrayList<>(100);

	public boolean add(String e) {
		return array.add(e);
	}

	public void clear() {
		array.clear();
	}

	public String get(int i) {
		return array.get(i);
	}

	public int indexOf(String search) {
		return array.indexOf(search);
	}

	public boolean remove(int i) {
		return array.remove(i) != null;
	}

	public boolean remove(String search) {
		for (int i = 0; i < array.length; ++i) {
			if (array[i] != null && array[i].toUpperCase().equals(search.toUpperCase())) {
				array[i] = null;
				return true;
			}
		}
		return false;
	}

	public int size() {
		int count = 0;
		for (int i = 0; i < array.length; ++i) {
			if (array[i] != null) {
				++count;
			}
		}
		if (count < 0 || count > Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		}
		return count;
	}

	public String toString() {
		String arrayString = "[ ";
		if (this.size() > 0) {
			for (int i = 0; i < array.length; i++) {
				if (array[i] != null) {
					if (arrayString.contentEquals("[ ")) {
						arrayString = arrayString + array[i] + " ";
					} else {
						arrayString = arrayString + "," + array[i] + " ";
					}

				}
			}
		} else {
			return "Array empty";
		}
		arrayString = arrayString + " ]";
		return arrayString;

	}
}
