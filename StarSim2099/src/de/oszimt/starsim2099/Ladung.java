package de.oszimt.starsim2099;

/**
 * @author Christoph Weller <DocGurk@gmail.com>
 * @version 10/09/2019
 */
public class Ladung extends Vektor2D {

	// Attribute
	private String typ;
	private int masse;
	
	// Methoden
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	public void setMasse(int masse) {
		this.masse = masse;
	}
	
	public String getTyp() {
		return this.typ;
	}
	
	public int getMasse() {
		return this.masse;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}