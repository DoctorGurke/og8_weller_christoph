package de.oszimt.starsim2099;

/**
 * @author Christoph Weller <DocGurk@gmail.com>
 * @version 10/09/2019
 */
public class Pilot extends Vektor2D {

	// Attribute
	private String grad;
	private String name;
	
	// Methoden
	
	public void setGrad(String grad) {
		this.grad = grad;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGrad() {
		return this.grad;
	}
	
	public String getName() {
		return this.name;
	}
	
}
