package de.oszimt.starsim2099;

/**
 * @author Christoph Weller <DocGurk@gmail.com>
 * @version 10/09/2019
 */
public class Planet extends Vektor2D {

	// Attribute
	private int anzahlHafen;
	private String name;
	
	// Methoden
	public void setAnzahlHafen(int anzahlHafen) {
		this.anzahlHafen = anzahlHafen;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAnzahlHafen() {
		return this.anzahlHafen;
	}
	
	public String getName() {
		return this.name;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
