package de.oszimt.starsim2099;

/**
 * @author Christoph Weller <DocGurk@gmail.com>
 * @version 10/09/2019
 */
public class Raumschiff extends Vektor2D {

	// Attribute
	private String typ;
	private String antrieb;
	private int maxLadekapazitaet;
	private int winkel;
	
	// Methoden
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}
	
	public void setMaxLadekapazitaet(int maxLadekapazitaet) {
		this.maxLadekapazitaet = maxLadekapazitaet;
	}
	
	public void setWinkel(int winkel) {
		if (winkel < 0) {
			winkel = 355;
		} else if (winkel > 360) {
			winkel = 5;
		}
		this.winkel = winkel;
	}

	public String getTyp() {
		return this.typ;
	}
	
	public String getAntrieb() {
		return this.antrieb;
	}
	
	public int getMaxLadekapazitaet() {
		return this.maxLadekapazitaet;
	}
	
	public int getWinkel() {
		return this.winkel;
	}
	
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
