package de.oszimt.starsim2099;

/**
 * @author Christoph Weller <DocGurk@gmail.com>
 * @version 10/09/2019
 */

public class Vektor2D {
	protected double posX;
	protected double posY;

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

}
