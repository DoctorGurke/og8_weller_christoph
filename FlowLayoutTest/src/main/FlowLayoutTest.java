package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class FlowLayoutTest extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FlowLayoutTest frame = new FlowLayoutTest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FlowLayoutTest() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 296, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JButton btnNewButton = new JButton("Button1");
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Button2");
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Button3");
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Button4");
		contentPane.add(btnNewButton_3);
		
		JLabel lblNewLabel = new JLabel("Text Element");
		contentPane.add(lblNewLabel);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Radio Button");
		contentPane.add(rdbtnNewRadioButton);
		
		textField = new JTextField();
		contentPane.add(textField);
		textField.setColumns(10);
	}

}
