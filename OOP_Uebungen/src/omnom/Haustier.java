package omnom;

public class Haustier {
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private final int MAX_WERT = 100;
	private final int MIN_WERT = 0;
	
	private String name;

	Haustier() {
		this.hunger = MAX_WERT;
		this.muede = MAX_WERT;
		this.zufrieden = MAX_WERT;
		this.gesund = MAX_WERT;
	}

	Haustier(String name) {
		this.name = name;
		this.hunger = MAX_WERT;
		this.muede = MAX_WERT;
		this.zufrieden = MAX_WERT;
		this.gesund = MAX_WERT;
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (hunger >= MAX_WERT) {
			this.hunger = MAX_WERT;
		} else if (hunger <= MIN_WERT) {
			this.hunger = MIN_WERT;
		} else {
			this.hunger = hunger;
		}
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (muede >= MAX_WERT) {
			this.muede = MAX_WERT;
		} else if (muede <= MIN_WERT) {
			this.muede = MIN_WERT;
		} else {
			this.muede = muede;
		}
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden >= MAX_WERT) {
			this.zufrieden = MAX_WERT;
		} else if (zufrieden <= MIN_WERT) {
			this.zufrieden = MIN_WERT;
		} else {
			this.zufrieden = zufrieden;
		}
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		if (gesund >= MAX_WERT) {
			this.gesund = MAX_WERT;
		} else if (gesund <= MIN_WERT) {
			this.gesund = MIN_WERT;
		} else {
			this.gesund = gesund;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void fuettern(int anzahl) {
		setHunger(this.hunger + anzahl);
	}

	public void schlafen(int dauer) {
		setMuede(this.muede + dauer);
	}

	public void spielen(int dauer) {
		setZufrieden(this.zufrieden + dauer);
	}

	public void heilen() {
		setGesund(MAX_WERT);
	}

}
